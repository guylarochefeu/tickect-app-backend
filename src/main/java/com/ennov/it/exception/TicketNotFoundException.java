package com.ennov.it.exception;

@SuppressWarnings("serial")
public class TicketNotFoundException extends RuntimeException {
	

	public TicketNotFoundException() {
        super();
    }
	
	public TicketNotFoundException(final String message) {
	    super(message);
	}
    
	public TicketNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
