package com.ennov.it.exception;

@SuppressWarnings("serial")
public class EmailNotValidExeption extends RuntimeException {
	
	public EmailNotValidExeption() {
        super();
    }
	
	public EmailNotValidExeption(final String message) {
	    super(message);
	}

}
