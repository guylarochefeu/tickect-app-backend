package com.ennov.it.exception;

@SuppressWarnings("serial")
public final class UserAlreadyExistException extends RuntimeException {
	
	public UserAlreadyExistException() {
        super();
    }
	
	public UserAlreadyExistException(final String message) {
	    super(message);
	}
    
	public UserAlreadyExistException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
