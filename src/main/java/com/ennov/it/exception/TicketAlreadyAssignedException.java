package com.ennov.it.exception;

@SuppressWarnings("serial")
public final class TicketAlreadyAssignedException extends RuntimeException {
	
	public TicketAlreadyAssignedException() {
        super();
    }
	
	public TicketAlreadyAssignedException(final String message) {
	    super(message);
	}
    
	public TicketAlreadyAssignedException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
