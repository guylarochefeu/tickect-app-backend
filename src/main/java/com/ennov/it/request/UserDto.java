package com.ennov.it.request;


import com.ennov.it.annotation.ValidEmail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
	
	@ValidEmail
	private String email;
	private String username;

}
