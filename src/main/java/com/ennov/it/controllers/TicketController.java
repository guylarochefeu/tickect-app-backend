package com.ennov.it.controllers;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.ennov.it.entities.Ticket;
import com.ennov.it.exception.TicketAlreadyAssignedException;
import com.ennov.it.exception.TicketNotFoundException;
import com.ennov.it.exception.UserNotFoundException;
import com.ennov.it.request.TicketDto;
import com.ennov.it.response.DataResponse;
import com.ennov.it.response.ListDataResponse;
import com.ennov.it.services.TicketServices;

import io.swagger.annotations.Api;

@RestController
@RequestMapping(produces = "application/json",value = "/ennov")
@Api(value = "Ticket Rest API", description = "Définition des API des Tickets")
public class TicketController {
	
	@Autowired
	private TicketServices ticketServices;
	
	@GetMapping("/tickets")
	public ListDataResponse<Ticket> findAllTickets(){
		return new ListDataResponse<Ticket>(HttpStatus.OK.value(),ticketServices.findAllTickets());
	}
	
	@GetMapping("/page/tickets")
	public DataResponse<Page<Ticket>> findAllTickets(@RequestParam Optional<String> titre,@RequestParam Optional<Integer> page,@RequestParam Optional<Integer> size) throws InterruptedException{
		TimeUnit.SECONDS.sleep(3);
		return new DataResponse<Page<Ticket>>(HttpStatus.OK.value(),ticketServices.getTicketConainsTitre(titre.orElse(""),page.orElse(0),size.orElse(10)));
	}
	
	@GetMapping("/tickets/{id}")
	public DataResponse<Ticket> findTicketById(@PathVariable Long id){
		Ticket ticket = ticketServices.findTicketById(id);
		if(ticket == null) {
			return new DataResponse<Ticket>(HttpStatus.NOT_FOUND.value(), "This ticket is not found",null) ;
		}
		return new DataResponse<Ticket>(HttpStatus.OK.value(),ticket) ;	
	}
	
	@PostMapping("/tickets")
	public DataResponse<Ticket> saveTicket(@RequestBody TicketDto ticketDto){
		return new DataResponse<Ticket>(HttpStatus.CREATED.value(),"Tickect cree avec success",ticketServices.saveTicket(ticketDto));
	}
	
	@PutMapping("/tickets/{id}")
	public DataResponse<Ticket> updateTicket(@PathVariable Long id,@RequestBody TicketDto ticketDto){
		try {
			Ticket ticket = ticketServices.updateTicket(id, ticketDto);
			return new DataResponse<Ticket>(HttpStatus.OK.value(),"Ticket modifie avec success",ticket);
		}catch (TicketNotFoundException e) {
			// TODO: handle exception
			return new DataResponse<Ticket>(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(),null);
		}catch(Exception e) {
		    e.printStackTrace();
			return new DataResponse<Ticket>("An UnHandledException occured; please see the message ="+ e.getMessage());
		}
		
	}
	
	@PutMapping("/tickets/{id}/assign/{userId}")
	public DataResponse<Ticket> assignTicket(@PathVariable Long id,@PathVariable Long userId){
		try {
			Ticket ticket = ticketServices.assignTicket(id, userId);
			return new DataResponse<Ticket>(HttpStatus.OK.value(),"Ticket est assigner avec success",ticket);
		}catch (TicketNotFoundException | TicketAlreadyAssignedException | UserNotFoundException e) {
			// TODO: handle exception
			return new DataResponse<Ticket>(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(),null);
		}catch(Exception e) {
		    e.printStackTrace();
			return new DataResponse<Ticket>("An UnHandledException occured; please see the message ="+ e.getMessage());
		}
		
	}
	
	@DeleteMapping("/tickets/{id}")
	public DataResponse<Ticket> deleteTicket(@PathVariable Long id){
		try {
			ticketServices.deleteTicket(id);
			return new DataResponse<Ticket>(HttpStatus.OK.value(),"Ticket suprimer avec succes",null);
		}catch (TicketNotFoundException e) {
			// TODO: handle exception
			return new DataResponse<Ticket>(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(),null);
		}catch(Exception e) {
		    e.printStackTrace();
			return new DataResponse<Ticket>("An UnHandledException occured; please see the message ="+ e.getMessage());
		}
		
	}

}
