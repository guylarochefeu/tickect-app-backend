package com.ennov.it.controllers;

import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ennov.it.entities.Ticket;
import com.ennov.it.entities.User;
import com.ennov.it.exception.EmailNotValidExeption;
import com.ennov.it.exception.UserAlreadyExistException;
import com.ennov.it.exception.UserNotFoundException;
import com.ennov.it.request.UserDto;
import com.ennov.it.response.DataResponse;
import com.ennov.it.response.ListDataResponse;
import com.ennov.it.services.TicketServices;
import com.ennov.it.services.UserService;

import io.swagger.annotations.Api;

@RestController
@RequestMapping(produces = "application/json",value = "/ennov")
@Api(value = "User Rest API", description = "Définition des API d'utilisteur")
public class UserController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private TicketServices ticketServices;
	
	@GetMapping("/users/{id}")
	public DataResponse<User> findUserById(@PathVariable Long id) {
		User user = userService.findUserById(id);
		if(user == null) {
			return new DataResponse<User>(HttpStatus.NOT_FOUND.value(), "The user not found",null) ;
		}
		return new DataResponse<User>(HttpStatus.OK.value(),user) ;		
	}
	
	@GetMapping("/users")
	public ListDataResponse<User> findUsers() {
		List<User> users = userService.findAllUsers();
		return new ListDataResponse<User>(HttpStatus.OK.value(),users);		
	}
	
	@PostMapping("/users")
	public DataResponse<User> createUser(@Valid @RequestBody UserDto userDto){
		try {
			User user = userService.registerUser(userDto);
			return new DataResponse<User>(HttpStatus.OK.value(),"Compte cree avec success",user);
		}catch (UserAlreadyExistException | EmailNotValidExeption e) {
			// TODO: handle exception
			return new DataResponse<User>(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(),null);
		}catch(Exception e) {
		    e.printStackTrace();
			return new DataResponse<User>("An UnHandledException occured;, please see the message ="+ e.getMessage());
		}
	}
	
	@PutMapping("/users/{id}")
	public DataResponse<User> updateUser(@PathVariable Long id, @RequestBody UserDto userDto){
		try {
			User user = userService.updateUser(id,userDto);
			return new DataResponse<User>(HttpStatus.OK.value(),"Compte modifie avec success",user);
		}catch (UserNotFoundException | UserAlreadyExistException | EmailNotValidExeption e) {
			// TODO: handle exception
			return new DataResponse<User>(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(),null);
		}catch(Exception e) {
		    e.printStackTrace();
			return new DataResponse<User>("An UnHandledException occured; please see the message ="+ e.getMessage());
		}
	}
	
	@GetMapping("/users/{id}/ticket")
	public ListDataResponse<Ticket> findAllTicketsByUser(@PathVariable Long id){
		try {
			List<Ticket> tickets = ticketServices.findTicketsByUserId(id);
			return new ListDataResponse<Ticket>(HttpStatus.OK.value(),tickets);
		}catch (UserNotFoundException | EmailNotValidExeption e) {
			// TODO: handle exception
			return new ListDataResponse<Ticket>(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(),null);
		}catch(Exception e) {
		    e.printStackTrace();
			return new ListDataResponse<Ticket>("An UnHandledException occured; please see the message ="+ e.getMessage());
		}
		
	}
	
	

}
