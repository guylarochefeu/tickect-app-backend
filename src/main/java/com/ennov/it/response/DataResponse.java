package com.ennov.it.response;

import lombok.Data;

@Data
public class DataResponse<T> {
  
	private int code;
    private T data;
    private String message;
	
	public DataResponse(int code, T data) {
        this.code = code;
        this.data = data;
    }
	public DataResponse(String message) {
        this.message = message;
    }
	
	public DataResponse(String message,T data) {
        this.message = message;
        this.data = data;
    }

    public DataResponse(int code, String message,T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
}
