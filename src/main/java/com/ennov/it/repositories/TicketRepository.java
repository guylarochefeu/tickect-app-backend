package com.ennov.it.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ennov.it.entities.Ticket;

@Repository
public interface TicketRepository extends JpaRepository<Ticket,Long> {
	
	@Query("select t " +
            "from Ticket as t " +
            "where t.user.recordId = :id")
	List<Ticket> findTicketsByUserId(@Param("id") Long id);
	
	Optional<Ticket> findTicketByTitre(String titre);

}
