package com.ennov.it.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ennov.it.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {
	Boolean existsByEmail(String email);
	Optional<User> findByEmail(String email);
}
