package com.ennov.it.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ennov.it.entities.User;

public interface UserPageRepository extends PagingAndSortingRepository<User,Long> {

}
