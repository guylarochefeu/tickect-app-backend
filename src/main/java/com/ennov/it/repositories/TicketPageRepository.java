package com.ennov.it.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.ennov.it.entities.Ticket;

@Repository
public interface TicketPageRepository extends PagingAndSortingRepository<Ticket,Long> {
	@Query("select t from Ticket as t where t.titre = :titre")
	Page<Ticket> getTicketLikeTitre(String titre,Pageable pageable);
	
	Page<Ticket> findByTitreContaining(String titre,Pageable pageable);

}
