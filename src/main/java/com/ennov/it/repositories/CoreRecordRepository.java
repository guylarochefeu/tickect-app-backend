package com.ennov.it.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ennov.it.entities.CoreRecord;

public interface CoreRecordRepository extends JpaRepository<CoreRecord,Long> {

}
