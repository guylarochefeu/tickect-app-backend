package com.ennov.it.entities;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "TICKET")
@DiscriminatorValue("RecordTicket")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ticket extends CoreRecord implements Serializable {
	
	@Column(name="TITRE",nullable = false)
	private String titre;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private User user;
	
	@Column(name = "ASSIGN_DATE")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date assignDate;

}
