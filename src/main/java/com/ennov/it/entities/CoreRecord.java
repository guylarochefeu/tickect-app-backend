package com.ennov.it.entities;

import java.io.Serializable;
import java.util.Date;

import com.ennov.it.enums.RecordStatus;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

 @Entity
 @Table(name="CORE_RECORD")
 @Inheritance(strategy = InheritanceType.JOINED)
 @DiscriminatorColumn(name = "RECORD_TYPE")
 @Data
 @AllArgsConstructor
 @NoArgsConstructor
public class CoreRecord implements Serializable {
	@Id
	@Column(name="RECORD_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long recordId;
	
	@Column(name="RECORD_CREATEDATE")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date recordCreatedate;
	
	@Column(name="RECORD_UPDATEDATE")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date recordUpdatedate;
	

	@Enumerated(EnumType.STRING)
	private RecordStatus recordState;

}
