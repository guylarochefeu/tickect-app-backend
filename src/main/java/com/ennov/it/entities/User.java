package com.ennov.it.entities;

import java.io.Serializable;
import java.util.List;

import com.ennov.it.enums.RecordRole;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "USERS")
@DiscriminatorValue("RecordUser")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User extends CoreRecord implements Serializable {
	
	@Column(name="USERNAME",nullable = false)
	private String username;
	
	@Column(name="EMAIL")
	private String email;
	@OneToMany(mappedBy ="user",fetch = jakarta.persistence.FetchType.LAZY)
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<Ticket> tikets;
	@Enumerated(EnumType.STRING)
	private RecordRole role;
	

}
