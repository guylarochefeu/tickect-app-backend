package com.ennov.it;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class EnnovTicketAppBackEndApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnnovTicketAppBackEndApplication.class, args);
	}

}
