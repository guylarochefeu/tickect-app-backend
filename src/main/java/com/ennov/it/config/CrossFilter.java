package com.ennov.it.config;

import java.io.IOException;


import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CrossFilter implements jakarta.servlet.Filter {

	@Override
	public void doFilter(jakarta.servlet.ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletResponse resp = (HttpServletResponse) response;
		
		  HttpServletRequest req = (HttpServletRequest) request;
		  
		  resp.setHeader("Access-Control-Allow-Origin","http://localhost:4200");
		  resp.setHeader("Access-Control-Allow-Methods","POST, GET, OPTIONS, DELETE, PUT");
		  resp.setHeader("Access-Control-Max-Age","3600");
		  resp.setHeader("Access-control-Allow-Credentials", "true");
		  resp.setHeader("Access-control-Allow-Headers","x-requested-with, authorization,Content-Type,Authorization,credential,X-XSRF-TOKEN" );
		  
		  if("OPTION".equalsIgnoreCase(req.getMethod())) {
		  resp.setStatus(HttpServletResponse.SC_OK);
		  }else { 
			  chain.doFilter(request,response);
		  }
		 
		
	}
	
	public void destroy() {}

}
