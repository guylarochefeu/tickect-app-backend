package com.ennov.it.services.impl;

import java.util.Date;
import java.util.List;
import java.util.Objects;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import static  org.springframework.data.domain.PageRequest.of;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ennov.it.entities.Ticket;
import com.ennov.it.entities.User;
import com.ennov.it.enums.RecordStatus;
import com.ennov.it.exception.TicketAlreadyAssignedException;
import com.ennov.it.exception.TicketNotFoundException;
import com.ennov.it.exception.UserNotFoundException;
import com.ennov.it.repositories.TicketPageRepository;
import com.ennov.it.repositories.TicketRepository;
import com.ennov.it.repositories.UserRepository;
import com.ennov.it.request.TicketDto;
import com.ennov.it.services.TicketServices;
import com.ennov.it.services.UserService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
@Service
@Transactional
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class TicketServiceImpl implements TicketServices {
	
	@Autowired
	TicketRepository ticketRepository;
	
	@Autowired
	UserService userService;
	
	@Autowired
	TicketPageRepository ticketPageRepository;
	
	public TicketServiceImpl(TicketRepository ticketRepository,UserService userService) {
		this.ticketRepository = ticketRepository;
		this.userService = userService;
	}

	@Override
	public List<Ticket> findTicketsByUserId(Long userId) throws UserNotFoundException {
		// TODO Auto-generated method stub
		User user = userService.findUserById(userId);
		if(Objects.isNull(user)) {
			throw new UserNotFoundException("Utilisateur introuvable");
		}
		return ticketRepository.findTicketsByUserId(userId);
	}

	@Override
	public List<Ticket> findAllTickets() {
		// TODO Auto-generated method stub
		return ticketRepository.findAll();
	}

	@Override
	public Ticket findTicketById(Long ticketId) {
		// TODO Auto-generated method stub
		return ticketRepository.findById(ticketId).orElse(null);
	}

	@Override
	public Ticket saveTicket(TicketDto ticketDto) {
		// TODO Auto-generated method stub
		Ticket ticket = new Ticket();
		ticket.setRecordCreatedate(new Date());
		ticket.setDescription(ticketDto.getDescription());
		ticket.setRecordState(RecordStatus.IN_PROGRES);
		ticket.setTitre(ticketDto.getTitre());
		return ticketRepository.save(ticket);
	}

	@Override
	public Ticket updateTicket(Long ticketId, TicketDto ticketDto) throws TicketNotFoundException {
		// TODO Auto-generated method stub
		Ticket ticket = this.findTicketById(ticketId);
		if(Objects.isNull(ticket)) {
			throw new TicketNotFoundException("Ce Ticket est Introuvable ...");
		}
		ticket.setDescription(ticketDto.getDescription());
		ticket.setTitre(ticketDto.getTitre());
		ticket.setRecordUpdatedate(new Date());
		return ticketRepository.save(ticket);
	}

	@Override
	public synchronized Ticket assignTicket(Long ticketId, Long userId)
			throws TicketNotFoundException, TicketAlreadyAssignedException,UserNotFoundException {
		// TODO Auto-generated method stub
		Ticket ticket = this.findTicketById(ticketId);
		if(Objects.isNull(ticket)) {
			throw new TicketNotFoundException("Ce Ticket est Introuvable ...");
		}
		if(Objects.nonNull(ticket.getUser())) {
			throw new TicketAlreadyAssignedException("Ce Ticket est deja assigne ..");
		}
		User user = userService.findUserById(userId);
		if(Objects.isNull(user)) {
			throw new UserNotFoundException("Utilisateur introuvable");
		}
		ticket.setAssignDate(new Date());
		ticket.setUser(user);
		ticket.setRecordUpdatedate(new Date());
		return ticketRepository.save(ticket);
	}

	@Override
	public void deleteTicket(Long ticketId) throws TicketNotFoundException {
		// TODO Auto-generated method stub
		Ticket ticket = this.findTicketById(ticketId);
		if(Objects.isNull(ticket)) {
			throw new TicketNotFoundException("Ce Ticket est Introuvable ...");
		}
		ticketRepository.deleteById(ticketId);
		
	}

	@Override
	public Page<Ticket> getTicketConainsTitre(String titre, int page, int size) {
		// TODO Auto-generated method stub
		log.info("---Fetch ticket for page {} and size {} and titre {} --",page,size,titre);
		Pageable pageable = PageRequest.of(page, size);
		return ticketPageRepository.findByTitreContaining(titre,pageable);
	}

}
