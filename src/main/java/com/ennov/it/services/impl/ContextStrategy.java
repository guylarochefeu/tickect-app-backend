package com.ennov.it.services.impl;

import java.util.List;

import com.ennov.it.entities.Ticket;
import com.ennov.it.entities.User;
import com.ennov.it.services.FilterTicketStrategy;

public class ContextStrategy {
  private FilterTicketStrategy filterTicketStrategy;
  
  List<Ticket> filter(User user){
	 return  filterTicketStrategy.filterControl(user);
  }
 

public void setFilterTicketStrategy(FilterTicketStrategy filterTicketStrategy) {
	this.filterTicketStrategy = filterTicketStrategy;
}
  
  
}
