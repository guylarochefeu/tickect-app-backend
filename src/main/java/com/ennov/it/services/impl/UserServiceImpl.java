package com.ennov.it.services.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ennov.it.annotation.EmailValidator;
import com.ennov.it.entities.User;
import com.ennov.it.enums.RecordStatus;
import com.ennov.it.exception.EmailNotValidExeption;
import com.ennov.it.exception.UserAlreadyExistException;
import com.ennov.it.exception.UserNotFoundException;
import com.ennov.it.repositories.UserRepository;
import com.ennov.it.request.UserDto;
import com.ennov.it.services.UserService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public User registerUser(UserDto userDto) throws UserAlreadyExistException {
		// TODO Auto-generated method stub
		if(emailExists(userDto.getEmail())) {
			throw new UserAlreadyExistException("Un utilisateur existe deja avec cette email ....");
		}
		
		if(!EmailValidator.patternEmailMatch(userDto.getEmail())) {
			throw new EmailNotValidExeption("Cette adresse mail n est pas valid !!");
		}
		
		User user = new User();
		user.setEmail(userDto.getEmail());
		user.setRecordCreatedate(Calendar.getInstance().getTime());
		user.setRecordState(RecordStatus.ENABLE);
		user.setUsername(userDto.getUsername());
		return userRepository.save(user);
	}

	@Override
	public List<User> findAllUsers() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}

	@Override
	public User updateUser(Long userId, UserDto userDto) throws UserNotFoundException,EmailNotValidExeption {
		// TODO Auto-generated method stub
		Optional<User> user = userRepository.findById(userId);
		if(!user.isPresent()) {
			throw new UserNotFoundException("Utilisateur introuvable");
		}
		if(!EmailValidator.patternEmailMatch(userDto.getEmail())) {
			throw new EmailNotValidExeption("Cette adresse mail n est pas valid !!");
		}
		
		if(emailExists(userDto.getEmail()) && !user.get().getEmail().equals(userDto.getEmail())) {
			throw new UserAlreadyExistException("Un utilisateur existe deja avec cette email ....");
		}
		User usr = user.get();
		usr.setEmail(userDto.getEmail());
		usr.setUsername(userDto.getUsername());
		usr.setRecordUpdatedate(new Date());
		return userRepository.save(usr); 
	}

	@Override
	public User findUserByEmail(String email) {
		// TODO Auto-generated method stub
		Optional<User> userOptional = userRepository.findByEmail(email);
		return userOptional.orElse(null);
	}

	@Override
	public User findUserById(Long id) {
		// TODO Auto-generated method stub
		Optional<User> userOption = userRepository.findById(id);
		return userOption.orElse(null);
	}

	@Override
	public boolean emailExists(String email) {
		// TODO Auto-generated method stub
		return userRepository.existsByEmail(email);
	}
	

}
