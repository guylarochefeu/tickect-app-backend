package com.ennov.it.services;

import java.util.List;

import com.ennov.it.entities.Ticket;
import com.ennov.it.entities.User;

public interface FilterTicketStrategy {
	
	List<Ticket> filterControl(User userConnected);

}
