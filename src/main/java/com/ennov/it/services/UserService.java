package com.ennov.it.services;

import java.util.List;

import com.ennov.it.entities.User;
import com.ennov.it.exception.EmailNotValidExeption;
import com.ennov.it.exception.UserAlreadyExistException;
import com.ennov.it.exception.UserNotFoundException;
import com.ennov.it.request.UserDto;

public interface UserService {
  
	User registerUser(UserDto userDto) throws UserAlreadyExistException,EmailNotValidExeption;
	List<User> findAllUsers();
	User updateUser(Long userId,UserDto userDto) throws UserNotFoundException,EmailNotValidExeption;
    User findUserByEmail(String email);
	User findUserById(Long id);
	boolean emailExists(String email);
}
