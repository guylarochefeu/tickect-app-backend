package com.ennov.it.services;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ennov.it.entities.Ticket;
import com.ennov.it.exception.TicketAlreadyAssignedException;
import com.ennov.it.exception.TicketNotFoundException;
import com.ennov.it.exception.UserNotFoundException;
import com.ennov.it.request.TicketDto;

public interface TicketServices {
	
	
	List<Ticket> findTicketsByUserId(Long userId) throws UserNotFoundException;
	List<Ticket> findAllTickets();
	Ticket findTicketById(Long ticketId);
	Ticket saveTicket(TicketDto ticketDto);
	Ticket updateTicket(Long ticketId,TicketDto ticketDto) throws TicketNotFoundException;
	Ticket assignTicket(Long ticketId,Long userId) throws TicketNotFoundException,TicketAlreadyAssignedException,UserNotFoundException;
	void deleteTicket(Long ticketId) throws TicketNotFoundException;
	
	
	///
	Page<Ticket> getTicketConainsTitre(String titre,int page,int size);

}
