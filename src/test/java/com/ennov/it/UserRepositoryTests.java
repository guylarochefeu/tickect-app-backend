package com.ennov.it;

import com.ennov.it.entities.User;
import com.ennov.it.repositories.UserRepository;
import com.ennov.it.request.UserDto;
import com.ennov.it.services.UserService;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;


import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserRepositoryTests {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;

    @Test
    @Order(1)
    @Rollback(value = false)
    public void registerUserTest(){
        UserDto userDto = new UserDto();
        userDto.setEmail("guylarochefeu@gmail.com");
        userDto.setUsername("GuyLaroche");
        User user = userService.registerUser(userDto);
        assertThat(user.getRecordId()).isGreaterThan(0);
    }

    @Test
    @Order(2)
    public void findAllUsersTests(){
        List<User> users = userService.findAllUsers();
        assertThat(users.size()).isGreaterThan(0);
    }

    @Test
    @Order(3)
    public void findUserByEmailTest(){
        User user = userService.findUserByEmail("guylarochefeu@gmail.com");
        assertThat(user).isNotNull();
    }

    @Test
    @Order(4)
    @Rollback(value = false)
    public void updateUserTest(){
        User user = userService.findUserByEmail("guylarochefeu@gmail.com");
        if(Objects.nonNull(user)){
            user.setEmail("laroche@gmail.com");
            User user1 = userRepository.save(user);
            assertThat(user1.getEmail()).isEqualTo("laroche@gmail.com");
        }else{
            fail("User not found !!");
        }
    }
    @Test
    @Order(5)
    @Rollback(value = false)
    public void deleteUserTest(){
        User user = userService.findUserByEmail("laroche@gmail.com");
        if(Objects.nonNull(user)){
            userRepository.deleteById(user.getRecordId());
            User user1 = userService.findUserByEmail("laroche@gmail.com");
            assertThat(user1).isNull();
        }else {
            fail("User not found !!!");
        }

    }
}
