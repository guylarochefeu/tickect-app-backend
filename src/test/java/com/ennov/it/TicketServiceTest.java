package com.ennov.it;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import com.ennov.it.entities.Ticket;
import com.ennov.it.entities.User;
import com.ennov.it.exception.TicketAlreadyAssignedException;
import com.ennov.it.exception.TicketNotFoundException;
import com.ennov.it.exception.UserNotFoundException;
import com.ennov.it.repositories.TicketRepository;
import com.ennov.it.request.TicketDto;
import com.ennov.it.services.UserService;
import com.ennov.it.services.impl.TicketServiceImpl;


@ExtendWith(MockitoExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TicketServiceTest {
	
	@Autowired
	private UserService userService;
	
	@Mock private TicketRepository ticketRepository;
	private AutoCloseable autoCloseable;
	
	private TicketServiceImpl ticketServiceImpl;
	
	@BeforeEach
	void setUp(){
		autoCloseable = MockitoAnnotations.openMocks(this);
		ticketServiceImpl = new TicketServiceImpl(ticketRepository,userService);
		
		//underTest = new TicketServiceImpl();
	}
	
	@AfterEach
	void tearDown() throws Exception{
		autoCloseable.close();
	}
	
    @Test
    @Disabled
	public void findTicketsByUserId(Long userId) throws UserNotFoundException{
    	//User usr = new User("laroche", "guy@gmail.com", null);
    	//usr.setRecordId(1L);
    	//when(ticketRepository.findTicketsByUserId(1L)).thenReturn(Stream.of(new Ticket("App1", "le brut",usr , null),new Ticket("App2", "le brut2",usr , null)).collect(Collectors.toList()));
       // assertThat(2).isEqualTo(ticketServiceImpl.findTicketsByUserId(1L).size());
    }
    
    @Test
	public void findAllTicketsTests() {
    	//when
    	ticketServiceImpl.findAllTickets();
    	//then
    	verify(ticketRepository).findAll();
    }
    
    @Test
	public void findTicketById() {
    	Ticket ticket = new Ticket();
    	ticket.setRecordId(1L);
		when(ticketRepository.findById(ticket.getRecordId())).thenReturn(Optional.of(ticket));
		Ticket expected = ticketServiceImpl.findTicketById(ticket.getRecordId());
		assertThat(expected).isSameAs(ticket);
		verify(ticketRepository).findById(ticket.getRecordId());
    }
    
    @Test
    @Order(1)
	public void saveTicketTest() {
    	TicketDto ticketDto = new TicketDto("TestMock","test demo");
    	ticketServiceImpl.saveTicket(ticketDto);
    	ArgumentCaptor<Ticket> ticketArgumentCaptor = ArgumentCaptor.forClass(Ticket.class);
    	verify(ticketRepository).save(ticketArgumentCaptor.capture());
    	Ticket captureTicket = ticketArgumentCaptor.getValue();
    	assertThat(captureTicket.getTitre()).isEqualTo(ticketDto.getTitre());
    }
    
    @Test
    @Disabled
	public void updateTicket() throws TicketNotFoundException{
    	TicketDto ticketDto = new TicketDto("TestMock1","test demo1");
    	ticketServiceImpl.updateTicket(1L,ticketDto);
    	ArgumentCaptor<Ticket> ticketArgumentCaptor = ArgumentCaptor.forClass(Ticket.class);
    	verify(ticketRepository).save(ticketArgumentCaptor.capture());
    	Ticket captureTicket = ticketArgumentCaptor.getValue();
    	assertThat(captureTicket.getTitre()).isEqualTo(ticketDto.getTitre());
    
    }
    
    @Test
    @Disabled
	public void assignTicket() throws TicketNotFoundException,TicketAlreadyAssignedException,UserNotFoundException{}
    
    @Test
	public void deleteTicket() throws TicketNotFoundException{
    	Ticket ticket = new Ticket();
    	ticket.setTitre("APP");
    	ticket.setRecordId(1L);
    	when(ticketRepository.findById(ticket.getRecordId())).thenReturn(Optional.of(ticket));
    	ticketServiceImpl.deleteTicket(ticket.getRecordId());
    	verify(ticketRepository).deleteById(ticket.getRecordId());
    }
}
