package com.ennov.it;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.ennov.it.request.UserDto;

import io.swagger.v3.core.util.Json;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void saveUserApiTest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.
				post("/ennov/users")
				.content(Json.pretty(new UserDto("guy@gmail.com","laroche")))
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
		        .andDo(print())
				.andExpect(status().isOk());
		
	}
	
	@Test
	public void findAllUserApiTest() throws Exception {
		mockMvc.perform(get("/ennov/users")).andExpect(status().isOk());
		
	}
	
	@Test
	public void findTicketByUserApiTest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.
				get("/ennov/users/{id}/ticket",1)
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
		        .andDo(print())
				.andExpect(status().isOk());
		
	}
	
	@Test
	public void updateUserApiTest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.
			   	put("/ennov/users/{id}",1)
				.content(Json.pretty(new UserDto("guy@gmail.com","laroche")))
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
		        .andDo(print())
				.andExpect(status().isOk());
		
	}

}
