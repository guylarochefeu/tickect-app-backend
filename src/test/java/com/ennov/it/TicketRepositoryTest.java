package com.ennov.it;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.ennov.it.entities.Ticket;
import com.ennov.it.repositories.TicketRepository;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

import java.util.List;
import java.util.Optional;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TicketRepositoryTest {
	
	@Autowired
	private TicketRepository ticketRepository;
    
	@Test
	@Order(1)
	@Rollback(value = false)
	public void saveTicketTest() {
		Ticket ticket = new Ticket();
		ticket.setTitre("TicketApp");
		ticket.setDescription("Application de gestion de ticket");
		Ticket t = ticketRepository.save(ticket);
		assertThat(t.getRecordId()).isGreaterThan(0);
		
	}
	
	@Test
	@Order(2)
	public void findAllTicketsTests() {
		List<Ticket> tickets = ticketRepository.findAll();
		assertThat(tickets.size()).isGreaterThan(0);
	}
	
	@Test
	@Order(3)
	public void findTicketByTitreTests() {
		Optional<Ticket> ticket = ticketRepository.findTicketByTitre("TicketApp");
        if(ticket.isPresent()) {
        	assertThat(ticket.get().getRecordId()).isGreaterThan(0);
        }else {
        	fail("Ticket introuvable");
        }
	}
	
	@Test
	@Order(4)
	public void UpdateTicketTests() {
		
		Optional<Ticket> ticket = ticketRepository.findTicketByTitre("TicketApp");
        if(ticket.isPresent()) {
        	Ticket t = ticket.get();
        	t.setTitre("TicketApp1");
        	Ticket ticket1 = ticketRepository.save(t);
        	assertThat(ticket1.getTitre()).isEqualTo("TicketApp1");
        }else {
        	fail("Ticket introuvable");
        }
		
		
	}
}
