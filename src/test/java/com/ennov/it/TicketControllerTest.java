package com.ennov.it;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.ennov.it.request.TicketDto;

import io.swagger.v3.core.util.Json;

@SpringBootTest
@AutoConfigureMockMvc
public class TicketControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void saveTicketApiTest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.
				post("/ennov/tickets")
				.content(Json.pretty(new TicketDto("Test","application de test")))
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
		        .andDo(print())
				.andExpect(status().isOk());
	}
	
	@Test
	public void findAllTicketsTest() throws Exception {
		mockMvc.perform(get("/ennov/tickets")).andExpect(status().isOk());
		
	}
	
	@Test
	public void findTicketsByIdApiTest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.
				get("/ennov/tickets/{id}",1)
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
		        .andDo(print())
				.andExpect(status().isOk());
		
	}
	
	@Test
	public void updateTicketApiTest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.
				put("/ennov/tickets/{id}",1)
				.content(Json.pretty(new TicketDto("Test123","application de test update")))
				.contentType(org.springframework.http.MediaType.APPLICATION_JSON)
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
		        .andDo(print())
				.andExpect(status().isOk());
	}
	
	@Test
	public void assignTicketForUserApiTest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.
				put("/ennov/tickets/{id}/assign/{useId}",1,2)
				.accept(org.springframework.http.MediaType.APPLICATION_JSON))
		        .andDo(print())
				.andExpect(status().isOk());
	}
	
	@Test
	public void deleteTicketTest() throws Exception {
		mockMvc.perform(delete("/ennov/tickets/{id}",1)).andExpect(status().isOk());
		
	}

}
