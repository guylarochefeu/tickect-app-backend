package com.ennov.it;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import com.ennov.it.entities.Ticket;
import com.ennov.it.entities.User;
import com.ennov.it.exception.EmailNotValidExeption;
import com.ennov.it.exception.UserAlreadyExistException;
import com.ennov.it.exception.UserNotFoundException;
import com.ennov.it.repositories.UserRepository;
import com.ennov.it.request.UserDto;
import com.ennov.it.services.impl.UserServiceImpl;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
	
    @Mock private UserRepository userRepository;
    private AutoCloseable autoCloseable;
    private UserServiceImpl userServiceImpl;
    
    @BeforeEach
	void setUp(){
		autoCloseable = MockitoAnnotations.openMocks(this);
		userServiceImpl = new UserServiceImpl(userRepository);
		
	}
	
	@AfterEach
	void tearDown() throws Exception{
		autoCloseable.close();
	}
	
	@Test
	public void registerUserTest() throws UserAlreadyExistException,EmailNotValidExeption{
		User user = new User();
		user.setEmail("guy@gmail.com");
		user.setUsername("laroche");
		
		UserDto userDto = new UserDto();
		userDto.setEmail("guy@gmail.com");
		userDto.setUsername("laroche");
		when(userRepository.save(ArgumentMatchers.any(User.class))).thenReturn(user);
		User created = userServiceImpl.registerUser(userDto);
		assertThat(created.getEmail()).isSameAs(user.getEmail());
		verify(userRepository).save(user);
	}
	
	@Test
	public void findAllUsers() {
		userServiceImpl.findAllUsers();
		verify(userRepository).findAll();
	}
	
	@Test
    @Disabled
	public void updateUser() throws UserNotFoundException,EmailNotValidExeption{
		User user = new User();
		user.setRecordId(1L);
		user.setUsername("francois");
		User newUser = new User();
		user.setUsername("jollo");
		//given(userRepository.findById(user.getRecordId()).get()).willReturn(Optional.of(user));
		ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
		userServiceImpl.updateUser(user.getRecordId(),new UserDto("marie@gmail.com",newUser.getUsername()));
		verify(userRepository).save(userArgumentCaptor.capture());
		verify(userRepository).findById(user.getRecordId());
	}
	
	@Test
    public void findUserByEmail() {
		userServiceImpl.findUserByEmail("guy@gmail.com");
		verify(userRepository).findByEmail("guy@gmail.com");
	}
	
	@Test
	public void findUserById() {
		User user = new User();
		user.setRecordId(1L);
		when(userRepository.findById(user.getRecordId())).thenReturn(Optional.of(user));
		User expected = userServiceImpl.findUserById(user.getRecordId());
		assertThat(expected).isSameAs(user);
		verify(userRepository).findById(user.getRecordId());
	}
	
	@Test
    @Disabled
	public void emailExists() {}
}
