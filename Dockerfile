FROM openjdk:17
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE=target/github-cicd-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} app-ci.jar
ENTRYPOINT ["java","-jar","/app-ci.jar"]